<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// api routes that need auth

Route::middleware(['auth:api'])->group(function () {


});

Route::get('home', 'HomeController@index');


/* routes for Cargos Controller  */	
	Route::get('cargos/', 'CargosController@index');
	Route::get('cargos/index', 'CargosController@index');
	Route::get('cargos/index/{filter?}/{filtervalue?}', 'CargosController@index');	
	Route::get('cargos/view/{rec_id}', 'CargosController@view');	
	Route::post('cargos/add', 'CargosController@add');	
	Route::any('cargos/edit/{rec_id}', 'CargosController@edit');	
	Route::any('cargos/delete/{rec_id}', 'CargosController@delete');



/* routes for FileUpload Controller  */	
Route::post('fileuploader/upload/{fieldname}', 'FileUploaderController@upload');
Route::post('fileuploader/s3upload/{fieldname}', 'FileUploaderController@s3upload');
Route::post('fileuploader/remove_temp_file', 'FileUploaderController@remove_temp_file');