
import { createRouter, createWebHashHistory } from 'vue-router';


function passRouteToProps(route){
	return {
		queryParams: route.query,
		fieldName: route.params.fieldName, 
		fieldValue: route.params.fieldValue
	}
}


let routes = [
	
		{
			path: '/', 
			alias: '/home',
			name: 'home' , 
			component: () => import('./pages/home/home.vue'),
			props: true
		},
		//Dashboard routes


//cargos routes
			{
				path: '/cargos/:fieldName?/:fieldValue?',
				name: 'cargoslist',
				component: () => import('./pages/cargos/list.vue'), 
				props: route => passRouteToProps(route)
			},
	
			{ 
				path: '/cargos/view/:id', 
				name: 'cargosview', 
				component: () => import('./pages/cargos/view.vue'), 
				props: true
			},
		
			{ 
				path: '/cargos/add', 
				name: 'cargosadd', 
				component: () => import('./pages/cargos/add.vue'), 
				props: true
			},
	
			{ 
				path: '/cargos/edit/:id', 
				name: 'cargosedit', 
				component: () => import('./pages/cargos/edit.vue'), 
				props: true
			},
		

	
	
	
	
	{ 
		path:  '/error/forbidden', 
		name: 'forbidden', 
		component: () => import('./pages/errors/forbidden.vue'),
		props: true
	},
	{ 
		path: '/error/notfound', 
		name: 'notfound',
		component: () => import('./pages/errors/pagenotfound.vue'),
		props: true
	},
	{
		path: '/:catchAll(.*)', 
		component: () => import('./pages/errors/pagenotfound.vue')
	}
];

export default () => {
	
	
	const router = createRouter({
		history: createWebHashHistory(),
		routes,
		scrollBehavior(to, from, savedPostion){
			if(savedPostion) return savedPostion;
			return { top:0 }
		}
	});
	
	return router;
}
